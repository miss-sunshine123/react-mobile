//引入routes
import routes from './router/routes';
// 引入useRoutes
import { useRoutes } from 'react-router-dom';
// 引入bottom
import Bottom from './components/layout/tabbar';
//引入suspense
import { Suspense } from 'react';
// 引入loading组件
import { SpinLoading } from 'antd-mobile';

function App() {
    // 使用useRoutes
    let element = useRoutes(routes);

    return (
        <div className="App">
            <Suspense fallback={<SpinLoading color="primary" />}>{element}</Suspense>
            <Bottom />
        </div>
    );
}

export default App;
