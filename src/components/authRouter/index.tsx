import React, { ReactNode, FC } from 'react';
import { Navigate } from 'react-router-dom';
let AuthRouter: FC<{ children?: ReactNode; tokenKey: string }> = ({ children, tokenKey }) => {
    //@ts-ignore
    let token = React.$local.get(tokenKey);
    if (token) {
        return <>{children}</>;
    } else {
        return <Navigate to="/login" />;
    }
};

export default AuthRouter;
