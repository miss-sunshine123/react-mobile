import React, { FC } from 'react';
import { NavBar, TabBar } from 'antd-mobile';
import { Route, useNavigate, useLocation, MemoryRouter as Router } from 'react-router-dom';
import { AppOutline, MessageOutline, UnorderedListOutline, UserOutline } from 'antd-mobile-icons';
// 引入样式
import './tabbar.css';

//定义图标
let Icon1: FC<any> = (active: boolean) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="25" height="25" filter="none">
            <g>
                <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" fill="rgba(21.93,132.09,251.94,1)"></path>
            </g>
        </svg>
    );
};
let Icon2: FC<any> = (active: boolean) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="22" filter="none">
            <g>
                <path d="M12 25.333h4v-8.077l-5.333-4.651-5.333 4.651v8.077h4v-5.333h2.667v5.333zM28 28h-24c-0.736 0-1.333-0.597-1.333-1.333v0-10.017c0-0 0-0 0-0 0-0.4 0.176-0.759 0.456-1.004l0.002-0.001 4.876-4.252v-6.059c0-0.736 0.597-1.333 1.333-1.333v0h18.667c0.736 0 1.333 0.597 1.333 1.333v0 21.333c0 0.736-0.597 1.333-1.333 1.333v0zM21.333 14.667v2.667h2.667v-2.667h-2.667zM21.333 20v2.667h2.667v-2.667h-2.667zM21.333 9.333v2.667h2.667v-2.667h-2.667zM16 9.333v2.667h2.667v-2.667h-2.667z" fill="rgba(96.9,100.98,111.94500000000001,1)"></path>
            </g>
        </svg>
    );
};
let Icon3: FC<any> = (active: boolean) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="22" height="22" filter="none">
            <g>
                <path d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z" fill="rgba(96.9,100.98,111.94500000000001,1)"></path>
            </g>
        </svg>
    );
};
const Bottom: FC = () => {
    let navigate = useNavigate();
    const location = useLocation();
    const { pathname } = location;

    const setRouteActive = (value: string) => {
        navigate(value);
    };

    const tabs = [
        {
            key: '/',
            title: '首页',
            icon: <Icon1 />,
        },
        {
            key: '/community',
            title: '社区',
            icon: <Icon2 />,
        },
        {
            key: '/mine',
            title: '我的',
            icon: <Icon3 />,
        },
    ];

    return (
        <TabBar activeKey={pathname} onChange={value => setRouteActive(value)}>
            {tabs.map(item => (
                <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
            ))}
        </TabBar>
    );
};

//导出组件
export default Bottom;
