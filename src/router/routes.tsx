// 引入react
import React from 'react';
// 引入路由组件
let Login = React.lazy(() => import(/*webpackChunkName:"login"*/ '../pages/login/index'));
let Home = React.lazy(() => import(/*webpackChunkName:"home"*/ '../pages/home/index'));
let Community = React.lazy(() => import(/*webpackChunkName:"community"*/ '../pages/community/index'));
let Mine = React.lazy(() => import(/*webpackChunkName:"mine"*/ '../pages/mine/index'));

// 定义路由类型
type RouteType = {
    path: string;
    element: React.ReactNode;
    children?: RouteType;
};
// 定义路由规则
let routes = [
    {
        path: '/',
        element: <Home />,
    },
    {
        path: '/login',
        element: <Login />,
    },
    {
        path: '/community',
        element: <Community />,
    },
    {
        path: '/mine',
        element: <Mine />,
    },
];

export default routes;
