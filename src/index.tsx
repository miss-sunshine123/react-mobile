import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';
// 引入lib-flexible
import 'lib-flexible/flexible.js';
// 引入browser-router
import { BrowserRouter as Router } from 'react-router-dom';
// 引入路由拦截器
import AuthRouter from './components/authRouter';
// 引入utils
import './utils';

// 引入Route和Routes
import { Route, Routes } from 'react-router-dom';
// 引入登录组件
import Login from './pages/login';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <Router>
        <AuthRouter tokenKey="token">
            <App />
        </AuthRouter>
        <Routes>
            <Route path="/login" element={<Login />} />
        </Routes>
    </Router>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
